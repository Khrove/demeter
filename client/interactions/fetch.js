const productInput = document.querySelector('[name="product"]');

productInput.addEventListener('keypress', function(e) {
    const key = e.which || e.keyCode || 0;
    if (key === 13) {
        console.log(e.currentTarget.value);
    }
});