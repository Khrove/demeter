const pptr = require('puppeteer');
const fire = require('../index');

async function getProductUrl(product) {
    let data = {
        createdAt: '',
        name: '',
        rating: '',
        cost: '',
        location: '',
        store: ''
    };

    const browser = await pptr.launch({
        headless: true
    });
    const page = await browser.newPage();
    await page.goto('https://www.wegmans.com');
    // await page.click('#search-btn');
    await page.type('#globalsearch', product);
    await page.click('body > header > noindex > div.mobile-search-tab-container.show-mobile.ng-scope > form > button.icon-icon-search.icon-search');

    await page.waitForSelector('body > div.container.main-wrapper > div > div > div:nth-child(4) > div > div > div:nth-child(1) > div.product-grid.row.grid-price.ng-scope > div > div > div.content-wrap > div:nth-child(1) > h5 > a');

    const anchorHref = await page.$eval('body > div.container.main-wrapper > div > div > div:nth-child(4) > div > div > div:nth-child(1) > div.product-grid.row.grid-price.ng-scope > div > div > div.content-wrap > div:nth-child(1) > h5 > a', span => span.getAttribute('href'));

    console.log(anchorHref);

    await page.goto(`https://www.wegmans.com/${anchorHref}`);
    await page.waitForSelector('body > div:nth-child(1) > div.container.main-wrapper.at-element-marker > div.product-detail-container > div:nth-child(3) > div.col-xs-12.col-sm-7.col-md-7.col-md-lg-6.col-lg-6.col-order-1.ng-scope > div.product-shortinfo-wrap.product-id > div > div.product-price-info > h4 > span');

    let element = await page.$("body > div:nth-child(1) > div.container.main-wrapper.at-element-marker > div.product-detail-container > div:nth-child(3) > div.col-xs-12.col-sm-7.col-md-7.col-md-lg-6.col-lg-6.col-order-1.ng-scope > div.product-shortinfo-wrap.product-id > div > div.product-price-info > h4 > span");
    let text = await (await element.getProperty('textContent')).jsonValue();

    data.cost = text;

    element = await page.$('body > div:nth-child(1) > div.container.main-wrapper.at-element-marker > div.product-detail-container > div:nth-child(3) > div.col-xs-12.col-sm-7.col-md-7.col-md-lg-6.col-lg-6.col-order-1.ng-scope > div.product-shortinfo-wrap.product-id > div > div.product-location > span');
    text = await (await element.getProperty('textContent')).jsonValue();

    data.location = text;

    element = await page.$('#averageRatingTemplate > div > a');
    text = await (await element.getProperty('textContent')).jsonValue();

    data.rating = text;

    element = await page.$('body > div:nth-child(1) > div.container.main-wrapper.at-element-marker > div.product-detail-container > div:nth-child(2) > div > div > h1');
    text = await (await element.getProperty('textContent')).jsonValue();

    data.name = text;

    await browser.close();

    data.createdAt = await createDate();
    console.group(data);
    await fire.createProduct(data);
};

async function createDate() {
    let today = new Date();
    let dd = String(today.getDate()).padStart(2, '0');
    let mm = String(today.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = today.getFullYear();

    return today = mm + '/' + dd + '/' + yyyy;
}

module.exports = {
    getProductUrl,
    createDate
};