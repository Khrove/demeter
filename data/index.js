const admin = require("firebase-admin");

const serviceAccount = require("./config/demeter-b1aef-firebase-adminsdk-qqofn-13193f70a4");

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://demeter-b1aef.firebaseio.com"
});

const db = admin.firestore();

async function getData(col, fieldPath, value) {
    const data = [];
    let ref = db.collection(col);
    let query = await ref.where(fieldPath, '==', value).get()
        .then(snapshot => {
            if(snapshot.empty) {
                console.log('No matching documents.');
                return;
            }

            snapshot.forEach(doc => {
                data.push(doc.data());
            });
        })
        .catch(err => {
            console.log('Error getting documents', err);
        });
    return data;
}

async function createStore(data) {
    let transaction = db.collection('stores')
        .add(data)
        .then((ref) => {
            console.log('Created a new store:', ref.id);
        });
}

async function createProduct(data) {
    data.store = 'U7BRfBIUgK88O5ZQ2Fym';
    let setDoc = db.collection('products').doc(data.name).set(data);
}

// getData('products', 'name', 'Chicken Caprese').then(function(res) {
//     console.log(res);
// });

module.exports = {
    createProduct,
    createStore,
    getData
};

